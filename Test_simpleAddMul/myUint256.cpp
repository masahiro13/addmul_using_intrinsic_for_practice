#include <iostream>

#include "myUint256_t.h"

std::ostream& operator<<(std::ostream& os, const my_uint256_t& a) {

	os << "[ " << a.X1.H << " : " << a.X1.L << " : " << a.X0.H << " : " <<	a.X0.L  << " ]";
	return os;
}

bool operator==(const my_uint256_t& a, const my_uint256_t& b) {

	return (a.X0 == b.X0) && (a.X1 == b.X1);
}

// c <- aL (aL || aH = a)
void l32bit(uint32_t& c, const uint64_t& a) {

	uint64_t m = 0x00000000FFFFFFFF;
	c = m & a;
}

// c <- aH (aL || aH = a)
void h32bit(uint32_t& c, const uint64_t& a) {

	c = a >> 32;
}

// c <- aL (aL || aH = a)
void l32bit2uint64_t(uint64_t& c, const uint64_t& a) {

	uint64_t m = 0x00000000FFFFFFFF;
	c = m & a;
}

// c <- aH (aL || aH = a)
void h32bit2uint64_t(uint64_t& c, const uint64_t& a) {

	c = a >> 32;
}



my_uint256_t addNaively(const my_uint128_t& a, const my_uint128_t& b) {

	my_uint256_t c;

	c.X0.L = a.L + b.L;

	c.X0.H = a.H + b.H;
	if (c.X0.H < a.H) ++c.X1.L;

	if (c.X0.L < a.L) {
		++c.X0.H;
		if (c.X0.H == 0) ++c.X1.L;
	}

	return c;
}

my_uint256_t mulNaively(const my_uint128_t& a, const my_uint128_t& b) {

	my_uint256_t c, tmp;
	my_uint128_t a0b0, a1b0, a0b1, a1b1;

	a0b0 = mulUint64_t(a.L, b.L);
	a1b0 = mulUint64_t(a.H, b.L);
	a0b1 = mulUint64_t(a.L, b.H);
	a1b1 = mulUint64_t(a.H, b.H);


	// c.X1.H || c.X1.L || c.X0.H = a1b0 + a0b1
	tmp = addNaively(a1b0, a0b1);
	c.X0.H = tmp.X0.L;
	c.X1.L = tmp.X0.H;
	c.X1.H = tmp.X1.L;

	// c.X0 += a0b0
	tmp = addNaively(c.X0, a0b0);
	c.X0 = tmp.X0;
	if (tmp.X1.L == 1) {
		++c.X1.L;
		if (c.X1.L == 0) ++c.X1.H;
	}

	// c.X1 += a1b1
	tmp = addNaively(c.X1, a1b1);
	c.X1 = tmp.X0;

	return c;
}

my_uint256_t addIntrinsic(const my_uint128_t& a, const my_uint128_t& b) {

	my_uint256_t c;

	uint32_t l32, h32;
	//uint32_t c_store[4];
	__declspec(align(32)) uint64_t c_store64[2];

	__m64 a_L64, a_H64, b_L64, b_H64;
	__m128i a128, b128, c128;

	l32bit(l32,a.L);
	h32bit(h32,a.L);
	a_L64 = _mm_set_pi32(h32, l32);

	l32bit(l32,a.H);
	h32bit(h32,a.H);
	a_H64 = _mm_set_pi32(h32, l32);

	a128 = _mm_set_epi64(a_L64, a_H64);


	l32bit(l32,b.L);
	h32bit(h32,b.L);
	b_L64 = _mm_set_pi32(h32, l32);

	l32bit(l32,b.H);
	h32bit(h32,b.H);
	b_H64 = _mm_set_pi32(h32, l32);

	b128 = _mm_set_epi64(b_L64, b_H64);


	c128 = _mm_add_epi64(a128, b128);
	_mm_storeu_si128( (__m128i*)c_store64, c128 );

/*
64bit x 2
*/
/* 下位-- c_store64[1] : c_store64[0] --上位 */

	c.X0.L = c_store64[1];
	c.X0.H = c_store64[0];

	if (c.X0.H < a.H) ++c.X1.L;

	if (c.X0.L < a.L) {
		++c.X0.H;
		if (c.X0.H == 0) ++c.X1.L;
	}



/*
32bit x 4
*/

/* 下位-- c_store[3] : c_store[2] : c_store[1] : c_store[0] --上位 */

#if 0
	c.X0.L = c_store[2];
	c.X0.L <<= 32;
	c.X0.L += c_store[3];

	c.X0.H = c_store[0];
	c.X0.H <<= 32;
	c.X0.H += c_store[1];
#endif



	return c;
}

my_uint256_t mulIntrinsic(const my_uint128_t& a, const my_uint128_t& b) {

	my_uint256_t c, tmp;
	my_uint128_t a0b0, a1b0, a0b1, a1b1;

	a0b0 = mulUint64_tIntrinsic(a.L, b.L);
	a1b0 = mulUint64_tIntrinsic(a.H, b.L);
	a0b1 = mulUint64_tIntrinsic(a.L, b.H);
	a1b1 = mulUint64_tIntrinsic(a.H, b.H);


	// c.X1.H || c.X1.L || c.X0.H = a1b0 + a0b1
	tmp = addNaively(a1b0, a0b1);
	c.X0.H = tmp.X0.L;
	c.X1.L = tmp.X0.H;
	c.X1.H = tmp.X1.L;

	// c.X0 += a0b0
	tmp = addNaively(c.X0, a0b0);
	c.X0 = tmp.X0;
	if (tmp.X1.L == 1) {
		++c.X1.L;
		if (c.X1.L == 0) ++c.X1.H;
	}

	// c.X1 += a1b1
	tmp = addNaively(c.X1, a1b1);
	c.X1 = tmp.X0;

	return c;
}
