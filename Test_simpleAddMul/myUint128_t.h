#ifndef __myUint128_t__
#define __myUint128_t__

#include <iostream>
#include <stdint.h>

struct my_uint128_t {

	uint64_t L, H;

	my_uint128_t() : L(0), H(0) {
	}

	friend std::ostream& operator<<(std::ostream&, const my_uint128_t&);
	friend bool operator==(const my_uint128_t&, const my_uint128_t&);

	void setRandom();
};

my_uint128_t mulUint64_t(uint64_t, uint64_t);
my_uint128_t mulUint64_tIntrinsic(uint64_t, uint64_t);



#endif