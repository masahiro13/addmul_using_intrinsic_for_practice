#include <iostream>
#include <stdint.h>
#include <random>

#include "myUint256_t.h"

void test() {

	my_uint128_t x;
	std::cout << x << "\n";

	my_uint256_t p;
	std::cout << p << "\n";



	uint64_t a, b;

	std::random_device rd;
	std::mt19937_64 mt(rd());

	a = mt();
	b = mt();

	x = mulUint64_t(a, b);

}

void testAVX() {

	__m256i X, Y;
	__m256d Z;
	uint32_t a32, b32;

	a32 = 0;
	b32 = 0xffffffff;

	X = _mm256_set_epi32(a32, b32, a32, b32, a32, b32, a32, b32);
	Y = _mm256_set_epi32(b32, a32, b32, a32, b32, a32, b32, a32);

	Z = _mm256_castsi256_pd(X);
}