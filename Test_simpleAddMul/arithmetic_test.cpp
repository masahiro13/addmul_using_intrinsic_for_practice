#include <iostream>
#include <stdint.h>
#include <random>

#include "myUint256_t.h"

#define Count 10

void addTest() {

	my_uint128_t a, b;
	a.L = 1;
	a.H = 1;
	b.L = -1;
	b.H = -1;

	std::cout << std::hex;
	std::cout << a << " + " << b << " = " << addNaively(a, b) << "\n";

	std::cout << a << " + " << b << " = " << addIntrinsic(a, b) << "\n";


	a.setRandom();
	b.setRandom();
	std::cout << a << " + " << b << " = " << addNaively(a, b) << "\n";
	std::cout << a << " + " << b << " = " << addIntrinsic(a, b) << "\n";

	my_uint256_t A, B;
	bool cmp;
	A = addNaively(a, b);
	B = addIntrinsic(a, b);

	cmp = (A == B);
	std::cout << "A: " << A << ", B: " << B << ", (A == B) : " << cmp << "\n";

	a.setRandom();
	A = addNaively(a, b);
	cmp = (A == B);
	std::cout << "A: " << A << ", B: " << B << ", (A == B) : " << cmp << "\n";



	uint64_t a64, b64;

	std::random_device rd;
	std::mt19937_64 mt(rd());

	a64 = 0xFFFFFFFFFFFFFFFF;
	b64 = 0xFFFFFFFFFFFFFFFF;
	a64 = mt();
	b64 = mt();

	std::cout << a64 << " *(naive ) " << b64 << " = " << mulUint64_t(a64, b64) << "\n";
	std::cout << a64 << " *(intrin) " << b64 << " = " << mulUint64_tIntrinsic(a64, b64) << "\n";
}

void mulTest() {

	my_uint128_t a, b;

	my_uint256_t A, B;
	bool cmp;

	a.L = 0xFFFFFFFFFFFFFFFF;
	a.H = 0xFFFFFFFFFFFFFFFF;
	b.L = 0xFFFFFFFFFFFFFFFF;
	b.H = 0xFFFFFFFFFFFFFFFF;

	a.setRandom();
	b.setRandom();
	A = mulNaively(a, b);
	B = mulIntrinsic(a, b);

	std::cout << a << " *(naive ) " << b << " = " << A << "\n";
	std::cout << a << " *(intrin) " << b << " = " << B << "\n";
}

void randomAddTest() {

	int correct, error;
	my_uint128_t a, b;

	my_uint256_t A, B;
	bool cmp;

	correct = 0;
	error = 0;
	for (unsigned int i = 0; i < Count; ++i) {

		a.setRandom();
		b.setRandom();
		A = addNaively(a, b);
		B = addIntrinsic(a, b);
		cmp = (A == B);

		if (!cmp) {

			++error;
			std::cout << A << " (Naive) \neq " << B << " (Intrinsic)\n";
		}

		++correct;
		std::cout << A << " (Naive) == " << B << " (Intrinsic)\n";
	}

	std::cout << "correct: " << correct << " (/" << Count << ")\n";
	std::cout << "error: " << error << " (/" << Count << ")\n";

}

void randomMulTest() {

	int correct, error;
	my_uint128_t a, b;

	my_uint256_t A, B;
	bool cmp;

	correct = 0;
	error = 0;
	for (unsigned int i = 0; i < Count; ++i) {

		a.setRandom();
		b.setRandom();
		A = mulNaively(a, b);
		B = mulIntrinsic(a, b);
		cmp = (A == B);

		if (!cmp) {

			++error;
			std::cout << A << " (Naive) \neq " << B << " (Intrinsic)\n";
		}

		++correct;
		std::cout << A << " (Naive) == " << B << " (Intrinsic)\n";
	}

	std::cout << "correct: " << correct << " (/" << Count << ")\n";
	std::cout << "error: " << error << " (/" << Count << ")\n";

}