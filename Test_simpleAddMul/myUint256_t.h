#ifndef __myUint256_t__
#define __myUint256_t__

#include <xmmintrin.h>
#include <emmintrin.h>
#include <immintrin.h>
#include <smmintrin.h>

#include "myUint128_t.h"


struct my_uint256_t {

	my_uint128_t X0, X1;

	my_uint256_t() {}

	friend std::ostream& operator<<(std::ostream&, const my_uint256_t&);
	friend bool operator==(const my_uint256_t&, const my_uint256_t&);
};

void l32bit(uint32_t&, const uint64_t&);
void h32bit(uint32_t&, const uint64_t&);
void l32bit2uint64_t(uint64_t&, const uint64_t&);
void h32bit2uint64_t(uint64_t&, const uint64_t&);

my_uint256_t addNaively(const my_uint128_t&, const my_uint128_t&);
my_uint256_t mulNaively(const my_uint128_t&, const my_uint128_t&);
my_uint256_t addIntrinsic(const my_uint128_t&, const my_uint128_t&);
my_uint256_t mulIntrinsic(const my_uint128_t&, const my_uint128_t&);

#endif