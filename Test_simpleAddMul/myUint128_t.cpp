#include <iostream>
#include <random>

#include "myUint256_t.h"

std::ostream& operator<<(std::ostream& os, const my_uint128_t& a) {

	os << "[ " << a.H << " : " << a.L << " ]";
	return os;
}

bool operator==(const my_uint128_t& a, const my_uint128_t& b) {

	return (a.L == b.L) && (a.H == b.H);
}

void my_uint128_t::setRandom() {

	std::random_device rd;
	std::mt19937_64 mt(rd());

	this->L = mt();
	this->H = mt();
}

/*
a = [a1 : a0]
b = [b1 : b0]
*/
my_uint128_t mulUint64_t(uint64_t a, uint64_t b) {

	my_uint128_t c;
	uint64_t a0, a1, b0, b1, a0b0, a1b0, a0b1, a1b1, tmp;

	l32bit2uint64_t(a0, a);
	h32bit2uint64_t(a1, a);
	l32bit2uint64_t(b0, b);
	h32bit2uint64_t(b1, b);

	a0b0 = a0 * b0;
	a1b0 = a1 * b0;
	a0b1 = a0 * b1;
	a1b1 = a1 * b1;

    // c.H_L32 || c.L_H32 = a1b0 + a0b1
	tmp = a1b0 + a0b1;
	if (tmp < a1b0) c.H += 0x0000000100000000;

	c.L = tmp << 32;
	c.H += tmp >> 32;

	// c.L += a0b0
	c.L += a0b0;
	if (c.L < a0b0) ++c.H;

	// c.H += a1b1
	c.H += a1b1;

	return c;
}

my_uint128_t mulUint64_tIntrinsic(uint64_t a, uint64_t b) {

	my_uint128_t c;
	uint32_t a0, a1, b0, b1;
	uint64_t a0b0, a1b0, a0b1, a1b1, tmp;
	__m128i g128, h128, i128;
	__declspec(align(32)) uint64_t store64[2];

	l32bit(a0, a);
	h32bit(a1, a);
	l32bit(b0, b);
	h32bit(b1, b);

	//  a1*b0, a0*b1
	g128 = _mm_set_epi32(0, a1, 0, a0);
	h128 = _mm_set_epi32(0, b0, 0, b1);

	i128 = _mm_mul_epu32(g128, h128);
	_mm_storeu_si128( (__m128i*)store64, i128 );

	a0b1 = store64[0];
	a1b0 = store64[1];

	//  a0*b0, a1*b1
	g128 = _mm_set_epi32(0, a1, 0, a0);
	h128 = _mm_set_epi32(0, b1, 0, b0);

	i128 = _mm_mul_epu32(g128, h128);
	_mm_storeu_si128( (__m128i*)store64, i128 );

	a0b0 = store64[0];
	a1b1 = store64[1];



    // c.H_L32 || c.L_H32 = a1b0 + a0b1
	tmp = a1b0 + a0b1;
	if (tmp < a1b0) c.H += 0x0000000100000000;

	c.L = tmp << 32;
	c.H += tmp >> 32;

	// c.L += a0b0
	c.L += a0b0;
	if (c.L < a0b0) ++c.H;

	// c.H += a1b1
	c.H += a1b1;

	return c;
}